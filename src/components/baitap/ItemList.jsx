import React, { Component } from "react";
import Item from "./Item";

class ItemList extends Component {
  render() {
    return (
      <section className="pt-4">
        <div className="container px-lg-5">
          <div className="row">
            <Item />
            <Item />
            <Item />
            <Item />
          </div>
        </div>
      </section>
    );
  }
}

export default ItemList;

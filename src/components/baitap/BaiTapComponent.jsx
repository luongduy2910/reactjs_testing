import React, { Component } from "react";
import Header from "./Header";
import Banner from "./Banner";
import Item from "./Item";
import Footer from "./Footer";
import ItemList from "./ItemList";

class BaiTapComponent extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <ItemList />
        <Footer />
      </div>
    );
  }
}

export default BaiTapComponent;

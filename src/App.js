import './App.css';
import BaiTapComponent from './components/baitap/BaiTapComponent';

function App() {
  return (
    <div className="App">
      <BaiTapComponent/>
    </div>
  );
}

export default App;
